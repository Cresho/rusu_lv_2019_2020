# Raspoznavanje uzoraka i strojno učenje

Ak.godina 2019./2020.


## Laboratorijske vježbe

Ovaj repozitorij sadrži potrebne datoteke za izradu svake laboratorijske vježbe (npr. LV1/resources). Rješenje svake laboratorijske vježbe treba spremiti u odgovarajući direktorij (npr. LV1/solutions).


## Podaci o studentu:

Ime i prezime: Krešimir Matić

## counting_words.py

Izmjene:
    - raw_input naredba ne postoji i zamjenjena je sa input naredbom
    - fhand = open(fname), a ne open(fnamex)
    - u for petlji u else grananju treba pisati counts[words] += 1
    - u print naredbama nedostaju zagrade

## Opis vježbe i rješenja

Vježba služi za upoznavanje sa git-om i python-om. Za svaki zadatak u git-u postoje korak po korak upute za rad, a zadaci za python su dovoljno jednostavni za savladavanje.
Prvi zadatak je obična operacija množenja između dva korisnički unešena broja.
U drugom zadatku se koriste if grananja i try i except naredbe za jednostavne matematičke uvjete.
Treći zadatak je isti kao prvi, ali se operacija množenja treba definirati unutar funkcije.
Četvrti zadatak koristi kombinaciju beskonačne petlje i if grananja za unos brojeva koji se broje, uspoređuju najveći i najmanji, te zbrajaju. Za svaku radnju postoji ispis.
Peti zadatak zahtijeva upis imena datoteke te unutar petlje se provjerava spam filter sa line.split naredbom za "X-DSPAM-Confidence: " string. Zatim se izračunava srednja vrijednost.
Šesti zadatak također koristi datoteku, ali ne treba je korisnik upisati. Kroz 2 for petlje se ispituju email, odnosno spremaju se email domene i broji se koliko puta se ponavljaju i to sve putem rječnika.