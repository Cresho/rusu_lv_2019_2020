#Kratki opisi rješenja zadatak

Zadatak 1

Prvo se naredbom findall pronađu svi email-ovi te se split metodom u printu ispisuju samo dijelovi ispred @ znaka.

Zadatak 2

Na isti način se pronalaze svi email-ovi te se uz različite findall uvjete dohvaćaju email-ovi sa različitim uvjetima.

Zadatak 3

Stvaranjem polja sa nasumičnim vrijednostima random.randint metodom rješavamo prvi korak zadatka. Zatim u petlji se vrše distribucije za pripadne vrijednosti u polju te se te vrijednosti prikažu u grafu plt.hist metodom, gdje su muške osobe označene plavom, a ženske osobe crvenom bojom. Srednja vrijednost se izračunava np.average metodom posebno sa oba spola te se rezultat ispisiva na graf axvline metodom.

Zadatak 4

U np.random.randint metodu predaju se vrijednosti 1 i 7 za brojeve sa kockice, i broj 100 za toliko ponavljanja. Plt.hist metodom se prikazuju rezultati na graf.

Zadatak 5

pd.read_csv metodom pristupamo u mtcars.csv datoteku i spremamo je u varijablu cars. plt.scatter metodom se prikazuje ovisnost potrošnje o konjskim snagama predajom cars.mpg i cars.hp atributa. Minimalna, maksimalna i prosječna vrijednost se prikazuje sa min, max i average metodama.

Zadatak 6

Slika se učitava mpimg.imread meotodom te se pravi kopija img.copy() metodom. Dodaje se koeficijent svjetline od 1.25 te se imgCopy množi sa tim koeficijentom i kroz plt.imshow se prikazuju slike na ekran.
