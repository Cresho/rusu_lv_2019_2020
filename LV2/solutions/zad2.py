import re

fhand = open("mbox-short.txt")
datoteka = fhand.read()
mails = re.findall('[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}', datoteka)

for i in range(len(mails)):
  mails[i]=mails[i].split('@')[0]

dat=' '.join(mails)

mails_multiple_a = re.findall('\s\S*a\S*\s', dat)
mails_single_a = re.findall('\s[B-Zb-z.]*a[B-Zb-z.]*\s', dat)
mails_no_a = re.findall('\s[B-Zb-z]+\s', dat)
mails_num = re.findall('\s\S*[0-9]+\S*\s', dat)
mails_lc = re.findall('\s[a-z.]+\s', dat)

for mail in mails_no_a:
  print(mail.lstrip(' ').lstrip('<'))