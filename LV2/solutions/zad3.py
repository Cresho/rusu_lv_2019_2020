import numpy as np

numbers = np.array(np.random.randint(2, size=10))
mim, sigmam = 180, 7
mif, sigmaf = 167, 7

distributionsM, distributionsF = [], []

for num in numbers:
  if num==1:
    distributionsM.extend(np.random.normal(mim, sigmam, 1))
  else:
    distributionsF.extend(np.random.normal(mif, sigmaf,1))

print(numbers)
print(distributionsM)
print(distributionsF)

import matplotlib.pyplot as plt 

x = np.linspace(0, 10, num=10) 
plt.hist([distributionsM,distributionsF], color=['blue','red'])   
plt.xlabel('Visina') 
plt.ylabel('Broj pojedinaca') 
plt.title('Distribucija visine')
plt.show()

heightHM = np.average(distributionsM)
heightFM = np.average(distributionsF)

plt.figure()
plt.axvline(heightHM, color="blue")
plt.axvline(heightFM, color="red")
plt.xlabel("Prosjecna visina")
plt.show()