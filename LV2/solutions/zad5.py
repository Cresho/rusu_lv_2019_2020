import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

cars = pd.read_csv("mtcars.csv")

plt.scatter(cars.hp, cars.mpg, c = cars.wt)
plt.xlabel('Konjska snaga - HP')
plt.ylabel('Potrosnja - MPG')

print('Minimalna vrijednost potrosnje: ', min(cars.mpg))
print('Maksimalna vrijednost potrosnje: ', max(cars.mpg))
print('Prosjecna vrijednost potrosnje: ', np.average(cars.mpg))