import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

img = mpimg.imread('tiger.png')
imgCopy = img.copy()
brightnessCoefficient = 1.25
imgCopy *= brightnessCoefficient


imgplot = plt.imshow(img) 
plt.figure()
imgCopyPlot = plt.imshow(imgCopy)