import re

fhand = open("mbox-short.txt")
dat = fhand.read()
mails = re.findall('\S*@\S+', dat)
for mail in mails:
  print(mail.split('@')[0].lstrip('<'))