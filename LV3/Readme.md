--LV3 Izvještaj

--Zadatak 1

Pomoću predloška su izvedene sve tražene operacije, odnosno svi uvjeti iz zadatka se mogu naći unutar predloška ili jednostavnim Google pretraživanjem.

--Zadatak 2

Za drugi zadatak je potrebno istražiti bar plot i box plot funkcije, vidjeti im značenje te proučiti koje parametre primaju funkcije da bi se mogao rješiti ovaj zadatak.

--Zadatak 3

 Prva dva podzadatka zadatka 3 su dovoljno jednostavna i potreban je ispravak samo par linija koda, točnije potrebno je zalijepiti točan url za lokaciju i razdoblje koje se istražuje, zatim je potrebno isprintati 3 datuma za koje je bila najveća koncentracija PM10, a to se radi na sličan princip kao zadatak 1.
 U trećem podzadatku se opet upotrebljava bar plot te su tu mjeseci razdvojeni kroz for petlju i kroz if uvjete su spremljene vrijednosti za dane kad nije postojalo mjerenje.
 Četvrti podzadatak je jednostavan jer se samo mjere i uspoređuju 2 mjeseca, a vrijednosti se prikupljaju kroz 2 for petlje te se razlika prikaže pomoću box plotu.
 U petom podzadatku se razdvoje radni dani od vikenda te se se u for petljama skupljaju vrijednosti mjerenja posebno za radne dane i vikend te se konačne vrijednosti prikazuju pomoću histograma.