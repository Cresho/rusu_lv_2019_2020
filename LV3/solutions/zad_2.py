import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 

mtcars = pd.read_csv('mtcars.csv')
plt.figure()
plt.bar(['4','6','8'],[len(mtcars[mtcars==4]),len(mtcars[mtcars.cyl==6]),len(mtcars[mtcars.cyl==8])], color='magenta')

plt.figure()
plt.boxplot(x=(mtcars[mtcars.cyl==4].wt,mtcars[mtcars.cyl==6].wt,mtcars[mtcars.cyl==8].wt),labels=('4','6','8'))

plt.figure()
plt.bar(['Automatic','Manual'],[mtcars[mtcars.am==1].mpg.mean(),mtcars[mtcars.am==0].mpg.mean()],color='cyan')

plt.figure()
plt.plot(mtcars[mtcars.am==1].qsec,mtcars[mtcars.am==1].hp, 'r.')
plt.plot(mtcars[mtcars.am==0].qsec,mtcars[mtcars.am==0].hp, 'b*')
