import urllib
import urllib.request
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
import numpy as np

#zd31
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=1.1.2017&vrijemeDo=31.12.2017'

airQualityHR = urllib.request.urlopen(url).read()
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme)
df.plot(y='mjerenje', x='vrijeme');

df['vrijeme'] = pd.to_datetime(df['vrijeme'],utc=True)
df['vrijeme'] = df['vrijeme'].dt.date
df['month'] = pd.DatetimeIndex(df['vrijeme']).month
df['dayofweek'] = pd.DatetimeIndex(df['vrijeme']).dayofweek

#zd32
print(df.sort_values(by=['mjerenje'],ascending=False)['vrijeme'].head(3))

#zd33
index = np.arange(1,13,1)
empty = []
for i in range(1,13):   
    mjesec= df[df.month==i]
    if (i==1) or (i==3) or(i==5) or(i==7) or (i==8) or (i==10) or (i==12):
        empty.append(31-len(mjesec))  
    elif (i==4) or (i==6) or(i==9) or(i==11):
        empty.append(30-len(mjesec))  
    else:
        empty.append(28-len(mjesec)) 

plt.figure()
plt.bar(index,empty,0.5,color='black')  
plt.xlabel("Mjesec")
plt.ylabel("Broj dana bez unosa mjerenja")
plt.grid(linestyle="-")

#zd34
sijecanj=df[df.month==1]
lipanj=df[df.month==6]
sijecanjData= []
lipanjData = []

for i in sijecanj['mjerenje']:
    sijecanjData.append(i)
for i in lipanj['mjerenje']:
    lipanjData.append(i)
    
plt.figure()
plt.boxplot([sijecanjData, lipanjData], positions = [2,9]) 
plt.xlabel('Mjesec')
plt.ylabel('Koncentracija')

#zd35
radni_tj=[]
vikend_tj=[]
radni = df[(df.dayofweek == 0)|(df.dayofweek == 1)|(df.dayofweek == 2)|(df.dayofweek == 3)|(df.dayofweek == 4)]
vikend = df[(df.dayofweek == 5)|(df.dayofweek == 6)]
for i in vikend['mjerenje']:
    vikend_tj.append(i)
for i in radni['mjerenje']:
    radni_tj.append(i)
    
bins=30
plt.figure()
plt.hist([radni_tj,vikend_tj],bins,color = ['magenta','cyan'])
plt.xlabel("Koncentracija tijekom tjedna")
plt.ylabel("Broj čestica PM10")
plt.legend(['Radno','Vikend'],loc=1)