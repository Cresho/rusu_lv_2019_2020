import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 

mtcars = pd.read_csv('mtcars.csv') 
print(mtcars.sort_values('mpg').tail(5)) #zd11
print(mtcars[(mtcars.cyl==8)].sort_values('mpg').head(3)) #zd12
print(mtcars[mtcars.cyl==6].mpg.mean()) #zd13
print(mtcars[(mtcars.wt>=2.000)&(mtcars.wt<=2.200)&(mtcars.cyl==4)].mpg.mean())  #zd14
print(len(mtcars[mtcars.am==1]),len(mtcars[mtcars.am==0])) #zd15
print(len(mtcars[(mtcars.am==1)&(mtcars.hp>100)])) #zd16
print(mtcars.wt*1000/2.2) #zd17