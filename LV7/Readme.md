Zadatak 1

U prvom zadatku je bilo potrebno kreirati model pomoću keras.Sequential() metode te definirati karakteristike mreže (metodom compile) i provesti učenje mreže (metodom fit). Također je bilo potrebno prikazati dobivene rezultate te model spremiti.

Zadatak 2

U drugom zadatku je bilo potrebno učitati model iz prethodnog zadatka i primijeniti ga na test.png sliku te prikazati rezultat. Kao drugi dio zadatka bilo je potrebo vlastoručno nacrtati broj na crnoj pozadini te modelom predvidjeti nacrtanu brojku.

Zadatak 3

U trećem zadatku je bilo potrebno opet klasificirati sliku sa CNN-om, te je to izvršeno kao i u prethodnim zadacima, te zapisati predikciju u varijablu label. Nažalost zadatak se nije mogao pokrenuti unutar colaba, ali svrha zadatka je bila korištenjem video kamere izdvajati pojedinačne brojeve nacrtane flomasterom na papiru.
