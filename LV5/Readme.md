Zadatak 1

Algoritam za grupiranje podataka, korišten u ovom zadatku je kmeans algoritam na nasumično generiranim podacima.

Zadatak 2 

U  ovom zadatku je također korišten kmeans algoritam za grupiranje podataka. U ovom zadatku je prikazano kako se, ovisno o parametru K, mijenja vrijednost kriterijske funkcije.

Zadatak 3

Za razliku od prvog i drugog zadatka, u trecem zadatku za grupiranje podataka korišteno je hijerarhijsko grupiranje.

Zadatak 4 i Zadatak 5

U ovim zadacima je pomoću kmeans algoritma izvršena kompresija, odnosno kvatizacija, slika.

