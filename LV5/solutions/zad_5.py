import scipy as sp 
from sklearn import cluster, datasets 
import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib.image as mpimg

img = mpimg.imread('LV5/resources/example.png')

try: 
 face = sp.face(gray=True) 
except AttributeError: 
 from scipy import misc 
 face = misc.face(gray=True) 

X = img.reshape((-1, 1))  
k_means = cluster.KMeans(n_clusters=10,n_init=1) 
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze() 
labels = k_means.labels_ 
img_compressed = np.choose(labels, values) 
img_compressed.shape = img.shape

plt.figure(1)
plt.title('Originalna slika') 
plt.imshow(img)

plt.figure(2)
plt.title('Kvantizirana slika')
plt.imshow(img_compressed)

plt.show()