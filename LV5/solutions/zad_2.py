from sklearn import datasets
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pyplot as plt

def generate_data(n_samples, flagc): 
 
 if flagc == 1: 
    random_state = 365 
    X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state) 
 
 elif flagc == 2: 
    random_state = 148 
    X,y = make_blobs(n_samples=n_samples, random_state=random_state) 
    transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]] 
    X = np.dot(X, transformation) 
 
 elif flagc == 3: 
    random_state = 148 
    X, y = make_blobs(n_samples=n_samples, 
    cluster_std=[1.0, 2.5, 0.5, 3.0], 
    random_state=random_state) 
 elif flagc == 4: 
    X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05) 
 elif flagc == 5: 
    X, y = datasets.make_moons(n_samples=n_samples, noise=.05) 
 else: 
    X = [] 
 
 return X

x = generate_data(500,2)
funVal = []
index = []
for i in range (2,20):
    kmeans = KMeans(n_clusters = i + 1, random_state = 0)
    kmeans.fit(x)
    funVal.append(kmeans.inertia_)
    index.append(i)

plt.scatter(index, funVal, color = 'black')
plt.show()

print(funVal)