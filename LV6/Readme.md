Zadatak 1

U prvom zadataku se generiraju nasumični podaci za treniranje i testiranje pomoću zadane ggenerate_data funkcije.

Zadatak 2

U druom zadatku se podaci plotaju pomoću scatter plot naredbe importane iz matplotlib.pyplot biblioteke.

Zadatak 3

U trećem zadatku je izgrađen model logističke regresije pomoću ugrađenih fit i predict metoda. Zatim je na scatter plotu iz prethodnog zadatka prikazana granica odluke.

Zadatak 4

Pomoću dobivenog koda je prikazan izlaz logističke regresije u obliku vjerojatnosti zajedno s podacima za učenje.

Zadatak 5

Provedena je klasifikacija testnog skupa podataka pomoću modela logističke regresije i metode predict.

Zadatak 6

Pomoću dobivenog koda iz predloška omogućeno je plotanje matrice zabune. Prvo se matrica zabune odredi pomoću confusion_matrix() metode iz sklearn.metrics biblioteke te su određene vrijednosti accuracy, misslasidfication rate, precision, recall i specificity pomoću varijabla dobivenih iz confusion_matrix() metode.

Zadatak 7

Ponovljen je cijeli postupak iz zadataka 3, 4, 5 i 6 uz novi model logističke regresije i novih podataka za treniranje.

Zadatak 8

Izgrađen je model metode K najbližih susjeda. Vrijednosti su standardizirane pomoću metode scale iz sklearn.preprocessing biblioteke te je sve plotano pomoću plot_KNN metode.