counter = 0
max = 0
min = 0
total = 0
while(1):
  try:
    unos=input("Unesi novi broj:")
    if unos == "Done":
      break
    unos=int(unos)
    total += unos
    if counter==0:
      max = unos
      min = unos
    else:
      if min > unos:
        min = unos
      elif max < unos:
        max = unos
    counter += 1
  except:
    print("NaN")

print('Uneseno je ', counter,' broj/a/eva.')
print('Najveci broj je: ', max)
print('Najmanji broj je: ', min)
print('Ukupni zbroj je', total)