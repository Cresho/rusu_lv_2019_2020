try:
  broj = float(input("Unesite broj od 0 do 1:"))
  if broj > 1 or broj < 0:
    print("Broj nije unutar intervala.")
  elif broj <= 1 and broj >= 0.9:
    print("A")
  elif broj < 0.9 and broj >= 0.8:
    print("B")
  elif broj < 0.8 and broj >= 0.7:
    print("C")
  elif broj < 0.7 and broj >= 0.6:
    print("D")
  else:
    print("F")
except:
  print("NaN")