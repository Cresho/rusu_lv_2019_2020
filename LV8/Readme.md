Zadatak 1

Svaka klasa se nalazi u posebnom direktoriju preko koje će model učiti podatke.

Zadatak 2

Uzet je testni uzorak izgradnjom novog direktorija unutar kojeg se nalaze direktoriji nazvanim odgovarajučim imenaima tako da odgovaraju imenima klasa, tj. imenima prometnih znakova.

Zadatak 3

U ovom zadatku je izgrađen model konvolucijske neuronske mreže koristeći se slikom iz predloška te je na kraju izračunata točnost ovog modela.