from tensorflow import keras
from keras import layers
from keras.models import Sequential
from keras.preprocessing import image_dataset_from_directory
import shutil
import os
import pandas as pd


test_dir_path = '/treffic-sign/'
test_dir_path_new = '/treffic-sign/Test_Dir/'

test_dir = pd.read_csv('/treffic-sign/Test.csv')
test_dir_lenght = test_dir.shape[0]

#---------------------zd2-------------------------
for i in range (0,test_dir_lenght):
   image_path = str(test_dir['Path'][i])
   class_id = str(test_dir['ClassId'][i])
   path = test_dir_path_new + class_id
   
   doesExist = os.path.exists(path)
   if not doesExist:
       os.makedirs(path)
   shutil.copy(test_dir_path + image_path, test_dir_path_new + class_id
               + '/' + image_path[4:])



#-----------------------zd3-----------------------
input_shape = (48, 48, 3)
num_classes = 43

train_data = image_dataset_from_directory(
 directory='treffic-sign/Train/',
 labels='inferred',
 label_mode='categorical',
 batch_size=32,
 image_size=(48, 48))


test_data = image_dataset_from_directory(
 directory='treffic-sign/Test_Dir/',
 labels='inferred',
 label_mode='categorical',
 batch_size=32,
 image_size=(48, 48))

sequential_model = Sequential([
      keras.Input(shape=input_shape),
      layers.Conv2D(32, kernel_size=(3, 3), activation="relu", padding="same"),
      layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
      layers.MaxPooling2D(pool_size=(2, 2)),
      layers.Dropout(0.2),
      layers.Conv2D(64, kernel_size=(3, 3), activation="relu", padding="same"),
      layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
      layers.MaxPooling2D(pool_size=(2, 2)),
      layers.Dropout(0.2),
      layers.Conv2D(128, kernel_size=(3, 3), activation="relu", padding="same"),
      layers.Conv2D(128, kernel_size=(3, 3), activation="relu"),
      layers.MaxPooling2D(pool_size=(2, 2)),
      layers.Dropout(0.2),
      layers.Flatten(),
      layers.Dense(512, activation="relu"),
      layers.Dropout(0.5),
      layers.Dense(num_classes, activation="softmax"),
])

sequential_model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['Accuracy'])
sequential_model.fit(train_data, epochs=5, batch_size=32)
sequential_model.summary()

accuracy = (sequential_model.evaluate(test_data, verbose=0))[1]
print("Točnost modela:", accuracy)
