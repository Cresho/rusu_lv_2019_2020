LV4 Opis rješenih zadataka

--Zadatak 1

U ovom zadatku je korišten linearni model te je za razumijevanje koda potrebno poznavanje svih funkcija i njihovih parametara proučavanjem predloška.

--Zadatak 2

Za rješavanje ovog zadatka potrebno je koristiti formulu iz predloška dana izrazom 4-10. 

--Zadatak 3

Zbog računske složenosti inverz operacije, korištene u prethodnom zadatku, u ovom zadatku se problem rješava metodom gradijentnog spusta.

--Zadatak 4

Za razliku od prvog zadatka u kojem se primjenjuje linearni model, četvrti zadatak se radi pomoću modela sa stupnjem većeg polinoma. Rezultati dobiveni ovim modelom bolje su aproksimirani u odnosu na model iz prvog zadatka.

--Zadatak 5

Za provjeru kvalitete polinomskih modela određenog stupnja koristi se srednja kvadratna pogreška, MSE. U ovom zadatku se se koriste polinomi 2., 6. i 15. stupnja. Pomoću rezultata dobivenih ispitivanjem srednje kvadratne pogreške testnog skupa i skupa za treniranje, moguće je odrediti je li došlo do pretjeranog prilagođavanja skupu za treniranje.

--Zadatak 6

U ovom zadatku se može primjetiti da različitim vrijednostima koeficijenta alfa ridge modela dobivamo različite i neprecizne rezultate. Model koji dobro opisuje podatke se dobije za vrijednosti alfa reda od 0.2.